package com.bbva.mmgp.lib.r015;

import java.util.Map;

public interface MMGPR015 {

	int executeInsert(Map<String,Object> transference);
	
}
